<?php

namespace Camino\Http\Controllers\Api;

use Illuminate\Http\Request;
use Camino\Http\Controllers\Controller;

class DataController extends Controller
{
    public function index(){

        $ciudades = [
            0=>'Rio Grande',
            1=>'Ushuaia',
            2=>'Tolhuin',
            3=>'Punta Arenas',
            4=>'Rio Gallegos',
            5=>'San Sebastian',
        ];
        $servicios = [
            0=>'Senda',
            1=>'Despegar',
            2=>'Otro',
            3=>'Pagina Web'
        ];
        $formas_pago = [
            0=>'Efectivo',
            1=>'Tarjeta',
            2=>'Cheque', 
            3=>'Transferencia',
            4=>'Deposito',
            5=>'A Credito',
            6=>'PayU',
        ];
        $vehiculo_categorias = [
            'a'=>'Auto Chico Cat A',
            'b'=>'Auto Mediano Cat B',
            'c'=>'Camioneta Chica Cat A',
            'd'=>'Camioneta Cat B'
        ];
        $lugares = [
            0=>'Aeropuerto RG' ,
            1=>'Aeropuerto USH',
            2=>'Vicente Lopez 225',
            3=>'RGA Parking',
            4=>'Senda Renting',
            5=>'Centro Rio Grande',
            6=>'Centro Ushuaia',
            7=>'Terminal RG',
            8=>'Terminal USH',
            9=>'Rio Grande',
            10=>'Ushuaia',
            11=>'Tolhuin',
            12=>'Otro'
        ];
        return response()->json([
            'lugares'=> $lugares,
            'servicios'=>$servicios,
            'formas_pago'=>$formas_pago,
            'vehiculo_categorias'=>$vehiculo_categorias,
            'ciudades'=>$ciudades,
        ],200);
    }
}
