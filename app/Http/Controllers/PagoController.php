<?php

namespace Camino\Http\Controllers;

use Camino\Pago;
use Illuminate\Http\Request;

class PagoController extends Controller
{
   
    public function store(Request $request)
    {
        
        \MercadoPago\SDK::setAccessToken("TEST_ACCESS_TOKEN"); // On Sandbox

        $token = $request->token;
        $payment_method_id = $request->payment_method_id;
        $installments = $request->installments;
        $issuer_id = $request->issuer_id;


        $payment = new \MercadoPago\Payment();
        $payment->transaction_amount = 143;
        $payment->token = $token;
        $payment->description = "Alquiler de auto";
        $payment->installments = $installments;
        $payment->payment_method_id = $payment_method_id;
        $payment->issuer_id = $issuer_id;
        $payment->payer = array(
        "email" => "felton_white@hotmail.com"
        );
        // Guarda y postea el pago
        $payment->save();
        //...
        // Imprime el estado del pago
        echo $payment->status;
    }
    
    
    public function success (){
        return response()->json('success :)');
    }
    public function failure (){
        return response()->json('failure :(');
    }
    public function pending (){
        return response()->json('pending :| ');
    }
    

}
