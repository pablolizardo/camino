<?php

namespace Camino\Http\Controllers;

use Camino\Tarifa;

class TarifaController extends Controller {

    public function __construct() { }

    public function index() {
        $tarifas = Tarifa::all();
        return response()->json($tarifas,200);
    }
}
