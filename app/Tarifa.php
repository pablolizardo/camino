<?php

namespace Camino;

use Illuminate\Database\Eloquent\Model;

class Tarifa extends Model
{
    protected $hidden = ['id', 'created_at','updated_at'];
}
