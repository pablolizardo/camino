<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTarifasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarifas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('cat_a')->default(2500)->nullable();
            $table->float('cat_b')->default(2900)->nullable();
            $table->float('cat_c')->default(3100)->nullable();
            $table->float('cat_d')->default(4000)->nullable();
            $table->float('cat_e')->default(5000)->nullable();
            $table->float('cat_f')->default(6000)->nullable();
            $table->float('dropoff')->default(3500)->nullable();
            $table->float('silla_bebe')->default(150)->nullable();

            $table->integer('recargo_tarjeta')->default(10)->nullable();
            $table->integer('descuento_vip')->default(20)->nullable();
            $table->integer('descuento_promo')->default(15)->nullable();
            $table->integer('cubiertas_invierno')->default(0)->nullable();
            $table->integer('segundo_conductor')->default(0)->nullable();
            $table->float('shuttle')->default(5000)->nullable();
            $table->float('transfer')->default(6500)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tarifas');
    }
}
