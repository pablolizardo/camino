<?php

use Illuminate\Database\Seeder;

class TarifasTableSeeder extends Seeder {
    public function run() {
    	DB::table('tarifas')->truncate();
	        DB::table('tarifas')->insert([
				'cat_a'	=>	2500,
				'cat_b'	=>	2900,
				'cat_c'	=>	3100,
				'cat_d'	=>	4000,
				'cat_e'	=>	5000,
				'cat_f'	=>	6000,
				'dropoff'	=>	3500,
				'silla_bebe'	=>	150,
				'recargo_tarjeta'	=>	10,
				'descuento_vip'	=>	20,
				'descuento_promo'	=>	15,
				'cubiertas_invierno'	=>	0,
				'segundo_conductor'	=>	0,
				'shuttle'	=>	5000,
				'transfer'	=>	6500,
	        ]);
        }

    }
