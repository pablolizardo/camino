<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        // DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->truncate();
        // DB::statement('SET FOREIGN_KEY_CHECKS=1');

        DB::table('users')->insert(array('name'      =>  'Operador', 'email'     =>  'tolhuin@camino.com.ar', 'password'  =>  \Hash::make('camino123')));
        DB::table('users')->insert(array('name'      =>  'Admin', 'email'     =>  'admin@camino.com.ar', 'password'  =>  \Hash::make('admin123')));
    }
}
